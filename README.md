# Peter Lawson's homework from Vanderbilt University FinTech Bootcamp (summer-fall 2020)

_Updated June 15, 2020_

_For details about the program, see [Program Info](#Program-Info) below._

This bootcamp has been exciting and inspiring. The fintech and data science frontier will continue to unfold and evolve in myriad ways. I'm eager to explore the real-world landscape with these powerful new tools and ways of thinking about business problems. I intend to maintain my current 3.9 GPA and complete the bootcamp with a broad, practical understanding of how coding and data analysis can be used to make more sophisticated and informed business decisions.

## Overview

This repo holds the majority of the work I did for the 24-week bootcamp. There were weekly individuals assignments and group projects; the group projects are housed in separate repos (links are provided).

There are several files in each of the weekly folders, so below I've provided links to the main notebook(s) containing my coded solution(s). The instructional file for each notebook can be found as a readme.md file in the same folder.

All assignments were made using [Jupyter](https://jupyter.org/) notebooks. Github will render these as static notebook layouts, so they typically take a few seconds to load when viewing in Github.

### Weeks 1 & 2: Python

[Solution part 1](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/02-Python/PyBank/main.ipynb)

[Solution part 2](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/02-Python/PyRamen/main.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/02-Python/README.md)

### Weeks 3 & 4: Pandas

[Whale analysis](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/04-Pandas/whale_analysis.ipynb)

(Instructions NA)

### Week 5: APIs

[Solution part 1](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/05-APIS/Instructions/Starter_Code/account_summary.ipynb)

[Solution part 1](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/05-APIS/Instructions/Starter_Code/portfolio_planner.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/05-APIS/Instructions/README.md)

### Week 6: PyViz

[Solution part 1](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/06-PyViz/Instructions/Starter_Code/rental_analysis.ipynb)

[Solution part 2](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/06-PyViz/Instructions/Starter_Code/dashboard.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/06-PyViz/Instructions/README.md)

### Week 7: SQL

[Solution](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/07-SQL/visual_data_analysis.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/07-SQL/Instructions/README.md)

### Weeks 8 & 9: Group project I

In this assignment there were several contributors, but my task was to build a module that, given a list of stock tickers (i.e. an investment portfolio), analyzes the predicted returns of the portfolio based on an aggregation of Monte Carlo simulations.

[Portfolio analysis notebook](https://github.com/VandyFinTech2020/Project01/blob/master/Project01-Portfolio-Analysis.ipynb)

### Week 10: Time series

[Solution part 1](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/10-TimeSeries/10-Time%20Series/Instructions/Starter_Code/time_series_analysis.ipynb)

[Solution part 2](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/10-TimeSeries/10-Time%20Series/Instructions/Starter_Code/regression_analysis.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/10-TimeSeries/10-Time%20Series/Instructions/README.md)

### Week 11: Machine Learning

[Solution part 1](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/11-Machine-Learning/Starter_Code/credit_risk_resampling.ipynb)

[Solution part 2](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/11-Machine-Learning/Starter_Code/credit_risk_ensemble.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/11-Machine-Learning/README.md)

### Week 12: NLP

[Solution](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/12-NLP/Instructions/Starter_Code/crypto_sentiment.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/12-NLP/Instructions/README.md)

### Week 13: AWS Lex & Lambda

[Solution](https://github.com/lawsonpd/unit13-challenge/blob/master/ClusteringCrypto/crypto_clustering.ipynb)

[Instructions](https://github.com/lawsonpd/fintech-bootcamp-homework/blob/master/13-AWS-Lex/Instructions/README.md)

_**Forthcoming:**_

### Week 14: Deep learning

### Week 15: Algorithmic trading

### Weeks 16 & 17: Group project II

### Week 18: Blockchain

### Week 19: Blockchain with Python

### Week 20: Smart contracts with Solidity

### Week 21: Advanced Solidity

### Week 22: Decentralized apps

### Weeks 23 & 24: Group project III

## Program Info

> The field of finance is evolving. Financial services firms, insurance agencies, and investment banks are all increasingly at the intersection of data and technology, harnessing algorithms, machine learning, big data, and blockchain to conduct business. Through hands-on classes in a convenient part-time format, Vanderbilt University FinTech Boot Camp gives students the knowledge they need to move toward the financial technology industry. With a project-based curriculum, students gain ample experience with a host of popular tools and methods such as Python programming, financial libraries, machine learning algorithms, Ethereum, and blockchain. [1][1]

### Curriculum

> Students will obtain marketable skills, learning how these fundamental concepts are leveraged within financial fields from financial planning to hedge funds, as well as best practices for using these skills to add value to an organization. The competitive curriculum covers: [1][1]

#### Financial Fundamentals

- Advanced Excel
- Time-Series Analysis
- Financial Ratios
- Financial Analysis

#### Blockchain and Cryptocurrency

- Solidity
- Ethereum
- Smart Contracts
- Consensus Algorithms
- Transactions
- Validation
- Distributed Ledger
- Cryptocurrency
- Truffle Suite
- Ganache

#### Financial Libraries and Tools

- NumPy
- SciPy
- Ffn
- Quantopian

#### Machine Learning Applications in Finance

- Algorithmic Trading
- Random Forests
- k-Nearest Neighbors (kNN)
- Support Vector Machines (SVM)
- Linear Regression
- Scikit-learn
- Financial Modeling
- Forecasting
- Logistic Regression

#### Financial Programming

- Python
- Pandas
- Matplotlib
- API Interactions
- Amazon Web Services
- NoSQL

[1]: https://bootcamps.vanderbilt.edu/fintech "Vanderbilt.edu"
